﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;

namespace OpenFileOrganizer
{
    class Program
    {
        private static int movedFiles { get; set; } = 0;
        private static int skippedFiles { get; set; } = 0;
        private static int deletedFiles { get; set; } = 0;


        private static bool skipissues { get; set; } = false;
        private static bool overrideall { get; set; } = false;



        static void Main(string[] args)
        {
            try
            {
                ParseArgs(args);
                
                ///
                /// Start Reading Directorys 
                ///
                var sourceDirectorys = new directorys();
                var serializer = new XmlSerializer(typeof(directorys));
                var buffer = Encoding.UTF8.GetBytes(File.ReadAllText(Environment.CurrentDirectory + @"\Directorys.xml"));

                ///
                /// Deserialize Directorys.xml to directorys class
                ///
                using (var stream = new MemoryStream(buffer))
                {
                    sourceDirectorys = (directorys)serializer.Deserialize(stream);
                }


                ///
                ///
                ///
                for(int i = 0; i < sourceDirectorys.source.Count(); i++ )
                {
                    if(Directory.Exists(sourceDirectorys.source[i]))
                    {
                        

                        string[] filenames = Directory.GetFiles(sourceDirectorys.source[i],"*.*", SearchOption.TopDirectoryOnly);
                        string[] files = GetFileNames(filenames);
                        Console.WriteLine("Start Organize: " + sourceDirectorys.source[i] + "   (Included "+ files.Length.ToString()+ " files)");

                        for (int y = 0; y < files.Count(); y++)
                        {
                            string firstchar = files[y];
                            string sourcefolder = sourceDirectorys.source[i] + @"\";
                            string destinationfolder = "";

                            if (char.IsNumber(firstchar[0]))
                            {
                                destinationfolder = FinalSlasher(FinalSlasher(sourcefolder) + "0-9");
                                CheckFolder(destinationfolder);
                                MoveFile(sourcefolder, destinationfolder, files[y]); 
                            }
                            else
                            {
                                destinationfolder = FinalSlasher(FinalSlasher(sourcefolder) + firstchar[0]);
                                CheckFolder(destinationfolder);
                                MoveFile(sourcefolder, destinationfolder, files[y]);
                            }
                        }

                        Console.WriteLine("End: " +sourceDirectorys.source[i] + " - is Done!");
                    }
                    else
                    {
                        Console.WriteLine("Directory does not exist: " + sourceDirectorys.source[i]);
                    }
                }

                Console.WriteLine("Completed!:" + 
                    Environment.NewLine + movedFiles.ToString() + " files moved " + 
                    Environment.NewLine + skippedFiles.ToString() + " files skipped" + 
                    Environment.NewLine + deletedFiles.ToString() + " files deleted!");

            }
            catch(Exception ex)
            {
                PrintException(ex);
            }

            Console.WriteLine("Press any key to close");
            Console.ReadKey();
        }

        /// <summary>
        ///     Returns Filenames without Path
        /// </summary>
        /// <param name="files"></param>
        /// <returns></returns>
        private static string[] GetFileNames(string[] files)
        {
            List<String> retval = new List<string>();
            for(int i = 0; i < files.Count(); i++)
            {
                retval.Add(Path.GetFileName(files[i]));
            }

            return retval.ToArray();
        }

        /// <summary>
        ///  Move File 
        /// </summary>
        /// <param name="source"></param>
        /// <param name="destination"></param>
        /// <param name="filename"></param>
        private static void MoveFile(string source, string destination, string filename)
        {
            if(Directory.Exists(source) && Directory.Exists(destination))
            {
                try
                {
                    if (!File.Exists(FinalSlasher(destination) + filename))
                    {
                        File.Move(FinalSlasher(source) + filename, FinalSlasher(destination) + filename);
                        Console.WriteLine(" -- " + FinalSlasher(source) + filename + " MOVED TO " + FinalSlasher(destination) + filename + " -- ");
                        movedFiles++;
                    }
                    else
                    {
                        if (!skipissues)
                        {

                            if (overrideall)
                            {
                                OverrideFile(FinalSlasher(source) + filename, FinalSlasher(destination) + filename);
                                deletedFiles++;
                            }
                            else
                            {

                                int i = 0;
                                ConsoleKeyInfo inputkey;
                                Console.WriteLine("File \"" + filename + "\" already exists. Do you want to override it?" + Environment.NewLine + "Press Y for Yes or Press N for No");
                                do
                                {
                                    if (i > 0)
                                        Console.WriteLine(Environment.NewLine + "===================================================");

                                    inputkey = Console.ReadKey();
                                    DefaultConsoleCompletion(inputkey);


                                    if (inputkey.Key != ConsoleKey.Y && inputkey.Key != ConsoleKey.N)
                                        Console.WriteLine("Wrong input! Press Y for Yes or Press N for No");

                                    i++;
                                } while (inputkey.Key != ConsoleKey.Y && inputkey.Key != ConsoleKey.N);

                                if (inputkey.Key == ConsoleKey.Y)
                                {
                                    try
                                    {
                                        File.Delete(FinalSlasher(destination) + filename);
                                        File.Move(FinalSlasher(source) + filename, FinalSlasher(destination) + filename);
                                        movedFiles++;
                                    }
                                    catch (Exception ex)
                                    {
                                        PrintException(ex);
                                    }
                                }
                                else if (inputkey.Key == ConsoleKey.N)
                                {
                                    Console.WriteLine("---------");
                                    Console.WriteLine("Do you want to delete the source file? Location: " + FinalSlasher(source) + filename + Environment.NewLine + "Press Y for Yes or Press N for No");
                                    int y = 0;
                                    do
                                    {
                                        if (y > 0)
                                            Console.WriteLine("---------");
                                        inputkey = Console.ReadKey();
                                        DefaultConsoleCompletion(inputkey);
                                        y++;

                                    } while (inputkey.Key != ConsoleKey.Y && inputkey.Key != ConsoleKey.N);



                                    if (inputkey.Key == ConsoleKey.Y)
                                    {
                                        try
                                        {
                                            File.Delete(FinalSlasher(source) + filename);
                                            deletedFiles++;
                                        }
                                        catch (Exception ex)
                                        {
                                            PrintException(ex);
                                        }
                                    }
                                    else if (inputkey.Key == ConsoleKey.N)
                                    {
                                        Console.WriteLine("---------");
                                    }
                                }


                                Console.WriteLine(Environment.NewLine + "===================================================");
                            }
                        }
                        else
                        {
                            skippedFiles++;
                        }

                    }
                }
                catch (Exception ex)
                {
                    PrintException(ex);
                }
            }

        }

        /// <summary>
        ///     Override existing file -
        ///     File will be deleted and moved again
        /// </summary>
        /// <param name="source"></param>
        /// <param name="destination"></param>
        private static void OverrideFile(string source, string destination)
        {
            try
            {
                File.Delete(destination);
                File.Move(source, destination);
            }
            catch (Exception ex)
            {
                PrintException(ex);
            }

        }

        /// <summary>
        ///     Some default Console output after user interaction
        /// </summary>
        /// <param name="keyinf"></param>
        private static void DefaultConsoleCompletion(ConsoleKeyInfo keyinf)
        {
            switch (keyinf.Key)
            {
                case ConsoleKey.Y:
                    Console.WriteLine("es");
                    break;
                case ConsoleKey.N:
                    Console.WriteLine("o");
                    break;
                default:
                    break;
            }

            if (keyinf.Key != ConsoleKey.Y && keyinf.Key != ConsoleKey.N)
                Console.WriteLine("Wrong input! Press Y for Yes or Press N for No");
        }

        /// <summary>
        ///  Add slash/Backslash if not exist
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        private static string FinalSlasher(string source)
        {
            if (source[source.Length - 1] != '\\' && source[source.Length - 1] != '/')
                return source + @"\";
            else
                return source;

        }

        /// <summary>
        ///     Check if Folder exists, If not create folder
        /// </summary>
        /// <param name="folder"></param>
        private static void CheckFolder(string folder)
        {
            if(!Directory.Exists(folder))
            {
                try
                {
                    Directory.CreateDirectory(folder);
                }
                catch (Exception ex)
                {
                    PrintException(ex);
                }
            }
        }

        /// <summary>
        ///  Print Exception to Console window
        /// </summary>
        /// <param name="ex"></param>
        private static void PrintException(Exception ex)
        {

            Console.WriteLine("Error: " + ex.Message + Environment.NewLine +
                    "ErrorCode: " + ex.HResult + Environment.NewLine +
                    "StackTrace: " + ex.StackTrace);
            Console.WriteLine("===============================================================================================================");
        }


        /// <summary>
        ///     Parse all Application Arguments
        /// </summary>
        /// <param name="args"></param>
        private static void ParseArgs(string[] args)
        {
            for(int i = 0; i < args.Length; i++)
            {
                switch(args[i])
                {
                    case "-skipissues":
                        skipissues = true;
                        break;
                    case "-help":
                        PrintHelp();
                        break;
                    case "-h":
                        PrintHelp();
                        break;
                    case "-?":
                        PrintHelp();
                        break;
                    case "?":
                        PrintHelp();
                        break;
                    case "-override":
                        overrideall = true;
                        break;

                }
            }
        }

        /// <summary>
        ///  Print Help text to command line
        /// </summary>
        private static void PrintHelp()
        {
            Console.WriteLine("WireframeZ - Open File Organizer Help");
            Console.WriteLine("Parameters: " + 
                                Environment.NewLine + "\t-skipissues \t\t Suppress all user interaction and skip override issues" +
                                Environment.NewLine + "\t-override \t\t Suppress all user interaction and override any file in destination folder (-skipissues will ignore this parameter)");
            Environment.Exit(0);
        }
    }
}

