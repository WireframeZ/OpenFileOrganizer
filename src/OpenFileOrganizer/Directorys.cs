﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace OpenFileOrganizer
{ 
    ///
    /// Container class for Directorys.xml reading
    /// 
    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
    public partial class directorys
    {

        private string[] sourceField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("source")]
        public string[] source
        {
            get
            {
                return this.sourceField;
            }
            set
            {
                this.sourceField = value;
            }
        }
    }


}
